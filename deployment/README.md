Get deployment
```bash
kubectl get deployment
```
```bash
kubectl get deployments.apps
```
Change settings of a deployment or create a new
```bash
kubectl apply -f deployment.yaml
```

Rollout
```bash
kubectl rollout undo deployment <deployment-name>
```

Describe deployment
```bash
kubectl describe deployment <deployment-name>
```

Run deployment with configmap
```bash
kubectl create -f configmap.yaml -f deployment-with-configmap.yaml
```

Port forward
```bash
kubectl port-forward <pod-name> 8080:80 &
```
& - run in background

Check forwarding
```bash
curl localhost:8080
```

Edit configmap
```bash
kubectl edit configmap <configmap-name>
```

Create secret
```bash
kubectl create secret generic <secret-name> --from-literal=<key>=<value>
```

Look at the secret
```bash
kubectl get secret <secret-name> -o yaml
```