ReplicaSet is a Kubernetes resource that ensures a specified number of pod replicas are running at any given time. 
It is a replacement for the deprecated ReplicationController.

labels - the labels are used to select the pods to be added to the replicaset

selectors - instruction to the replicaset to select the pods with the labels specified in the labels field

Creating a replicaset
```bash
kubectl create -f replicaset.yaml
```

List replicaset
```bash
kubectl get replicaset
```

```bash
kubectl get replicasets.apps
```

Change settings of a replicaset or create a new
```bash
kubectl apply -f replicaset.yaml
```

Scale replicaset
```bash
kubectl scale --replicas=2 replicaset <replicaset-name>
```

Describe replicaset
```bash
kubectl describe replicaset <replicaset-name>
```
