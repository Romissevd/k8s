Install auto-completion on Tab button

```bash
kubectl completion
```

Describe node
```bash
kubectl describe node
```

Information about the abstracted resources
```bash
kubectl explain <abstraction>.<command>
```