List storage classes
```bash
kubectl get storageclass
```

Create a PVC
```bash
kubectl apply -f pvc.yaml
```

Get PVC
```bash
kubectl get pvc
```

Get PV
```bash
kubectl get pv
```

Get manifest
```bash
kubectl get pv <pvc-name> -o yaml
```

Delete PVC
```bash
kubectl delete pvc <pvc-name>
```
