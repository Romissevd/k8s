To enable the NGINX Ingress controller, run the following command:

```bash
minikube addons enable ingress
```

Verify that the NGINX Ingress controller is running

```bash
kubectl get pods -n ingress-nginx
```

Verify the IP address is set:

```bash
kubectl get ingress
```

Verify that the Ingress controller is directing traffic:

```bash
curl --resolve "my-kube-app.info:80:$( minikube ip )" -i http://my-kube-app.info
```
