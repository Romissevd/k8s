DaemonSet
```bash
kubectl apply -f daemonset.yaml
```

List jobs
```bash
kubectl get jobs
```

Delete jobs
```bash
kubectl delete job <job-name>
```

List cronjobs
```bash
kubectl get cronjobs
```

Delete cronjobs
```bash
kubectl delete cronjobs <cronjob-name>
```