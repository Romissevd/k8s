List service accounts
```bash
kubectl get serviceaccounts
```

Check apply manifest
```bash
kubectl get sa <service-account-name> -o yaml
```

List cluster roles
```bash
kubectl get clusterroles
```

Get cluster role
```bash
kubectl get clusterrole <cluster-role-name> -o yaml
```

Request with role
```bash
kubectl get service --as=system:serviceaccount:<namespace>:<service-account-name>
```