Apply manifest

```bash
kubectl create -f first_pod.yaml
```

Get all pods

```bash
kubectl get pod
```

More information about the pods

```bash
kubectl get pods -o wide
```

Delete pod

```bash
kubectl delete pod <pod-name>
```

Delete all pods 

```bash
kubectl delete pod --all
```

Show labels

```bash
kubectl get pods --show-labels
```

Describe a pod

```bash
kubectl describe pod <pod-name>
```
