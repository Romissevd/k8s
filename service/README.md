Apply service
```bash
kubectl create -f <yaml-file>
```
or
```bash
kubectl apply -f <yaml-file>
```

Get service
```bash
kubectl get service
```
or
```bash
kubectl get svc
```

Endpoints
```bash
kubectl get endpoints
```